package com.example.helloWorld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;


public class MainActivity extends AppCompatActivity {

    // information qui apparaitra dans le log (à adapter en fonction des besoins)
    public static final String TAG = "MainActivity:LOG";
    // ou
    //public static final String TAG = MainActivity.class.getSimpleName();
    // ou autre combinaison String pour pouvoir la retrouver facilement dans le log...


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Affichage d'un message débug dans le log
        Log.d(MainActivity.TAG, "onCreate()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Affichage d'un message débug dans le log
        Log.d(MainActivity.TAG, "onStart()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // Affichage d'un message débug dans le log
        Log.d(MainActivity.TAG, "onRestart()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Affichage d'un message débug dans le log
        Log.d(MainActivity.TAG, "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Affichage d'un message débug dans le log
        Log.d(MainActivity.TAG, "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Affichage d'un message débug dans le log
        Log.d(MainActivity.TAG, "onDestroy()");
    }
}